
package test1.com.test;
public class Personne {
    private int matricule;
    private String nom;
    private int salaire;
    public Personne(int matricule, String nom, int salaire) {
        this.matricule = matricule;
        this.nom = nom;
        this.salaire = salaire;
    }
    public int getMatricule() {
        return matricule;
    }
    public void setMatricule(int matricule) {
        this.matricule = matricule;
    }
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public int getSalaire() {
        return salaire;
    }
    public void setSalaire(int salaire) {
        this.salaire = salaire;
    }
}
