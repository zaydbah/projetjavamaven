package test1.com.test;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
public class PersonneTest {
    //Indique que l'on est dans une classe de Test
    @Test
    public void constructeur(){
        test1.com.test.Personne personne=new test1.com.test.Personne(1,"villiers",1500);
        assertEquals(1,personne.getMatricule());
        assertEquals("villiers",personne.getNom());
        assertEquals(1500,personne.getSalaire());
    }
}